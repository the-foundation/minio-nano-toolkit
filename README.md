# MINIO nano helper toolkit for mc

## `mc-newuser.sh`  

* asks everything by command line ( or environment variables `AWS_USER` `AWS_AUTH` `AWS_BUCKET` ,optionally `MYTARGET`  which hast to match a target in mc config.json )
* filters non-alphanumeric chars from your input
* enforces minimum 24 character passwords
* adds a policy named username_usagestring
* creates a bucket usagestring-username
* creates a user and attaches a him/her to a group named username_usagestring
* applies the policy username_usagestring  to the user and a group called username_usagestring , so the user can have multiple bucket acces by groups

---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/minio-nano-toolkit/README.md/logo.jpg" width="480" height="270"/></div></a>
