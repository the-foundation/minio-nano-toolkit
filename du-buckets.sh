#!/bin/bash

if [ -z "$MYTARGET" ];then  MYTARGET=local;fi

test -x ./mc && MCBIN="./mc"
test -x /data/mc && MCBIN=/data/mc
which mc &>/dev/null && MCBIN=$(which mc)


which $MCBIN || exit "need mc binary"

$MCBIN ls local --json |sed 's/.\+key":"//g;s/".\+//g'|while read bucket ;do  $MCBIN du local/$bucket;done
