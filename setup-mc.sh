#!/bin/sh
test -d ~/.mc || mkdir ~/.mc 

echo '{
	"version": "9",
	"hosts": {
		"gcs": {
			"url": "https://storage.googleapis.com",
			"accessKey": "YOUR-ACCESS-KEY-HERE",
			"secretKey": "YOUR-SECRET-KEY-HERE",
			"api": "S3v2",
			"lookup": "dns"
		},
		"local": {
			"url": "http://localhost:9000",
			"accessKey": "'$(printenv MINIO_ACCESS_KEY)'",
			"secretKey": "'$(printenv MINIO_SECRET_KEY)'",
			"api": "S3v4",
			"lookup": "auto"
		},
		"play": {
			"url": "https://play.minio.io:9000",
			"accessKey": "Q3AM3UQ867SPQQA43P2F",
			"secretKey": "zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG",
			"api": "S3v4",
			"lookup": "auto"
		},
		"s3": {
			"url": "https://s3.amazonaws.com",
			"accessKey": "YOUR-ACCESS-KEY-HERE",
			"secretKey": "YOUR-SECRET-KEY-HERE",
			"api": "S3v4",
			"lookup": "dns"
		}
	}
}' > ~/.mc/config.json

