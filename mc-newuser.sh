#!/bin/sh

if [ -z "$MYTARGET" ];then  MYTARGET=local;fi

test -x ./mc && MCBIN="./mc"
test -x /data/mc && MCBIN=/data/mc
which mc && MCBIN=$(which mc)|| MCBIN=/data/mc

which $MCBIN || exit "need mc binary"
$MCBIN ls ${MYTARGET} || exit "please setup a target named ${MYTARGET} in ~/.mc/config.json"

if [ -z $AWS_USER ] ;then
    echo -n "USERID:";read usertmp;
    AWS_USER=$(echo "$usertmp"|tr -cd '[:alnum:]');
fi

if [ -z $AWS_USER ] ; then echo "no empty user please";exit 2 ;fi 

if [ -z $AWS_BUCKET ] ;then
    echo -n "ID-PREFIX(e.g. backup-restic):";read buckettmp;
if [ -z $buckettmp ] ; then echo "no empty bucket name please";exit 2 ;fi 
    AWS_BUCKET=$(echo "$buckettmp"|tr -cd '[:alnum:]-')"-"${AWS_USER};

    echo "I filtered everything except - and a-zA-Z0-9 , the names are now:"
    echo "USER: "${AWS_USER}"    BUCKET: "$AWS_BUCKET
fi


if [ -z $AWS_AUTH ] ;then
    echo 'enter PASSWORD FOR BUCKET(only [ letters numbers . - _ ] allowed):'
    read tmppass
    AWS_AUTH=$(echo "$tmppass"|tr -cd '[:alnum:]-');
    echo "I filtered everything except - and a-zA-Z0-9 , the AWS_SECRET_KEY is now:"
    echo ${AWS_AUTH}
fi

if [ "$(echo ${AWS_AUTH} |wc -c )" -lt 24 ] ; then echo "minimum 24 character pass please";exit 2 ;fi 
if [ -z $AWS_AUTH ] ; then echo "no empty pass please";exit 2 ;fi 


makepolicy=0
test -f ${AWS_USER}_${AWS_BUCKET}.json || makepolicy=1 && echo "policy already existing , will apply it to bucket , change with mc if necessary"

if [ "$makepolicy" -eq 1 ];then 

echo "enabling default read-write policy named ${AWS_USER}_${AWS_BUCKET}"

echo '{
    "Version": "2012-10-17",
    "Id": "'${AWS_USER}'",
    "Statement": [
        {
            "Sid": "ExampleStatement01",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::Account-ID:user/'${AWS_USER}'"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:GetBucketLocation",
                "s3:ListBucket",
                "s3:ListMultipartUploadParts",
                "s3:AbortMultipartUpload",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::'${AWS_BUCKET}'/*",
                "arn:aws:s3:::'${AWS_BUCKET}'"
            ]
        }
    ]
}' > ${AWS_USER}_${AWS_BUCKET}.json

fi

##IMPORT POLICY
${MCBIN} admin policy add ${MYTARGET} ${AWS_USER}_${AWS_BUCKET} ${AWS_USER}_${AWS_BUCKET}.json || ${MCBIN} admin policies add ${MYTARGET} ${AWS_USER}_${AWS_BUCKET} ${AWS_USER}_${AWS_BUCKET}.json
##CREATE BUCKET
${MCBIN} ls ${MYTARGET}/${AWS_BUCKET} |grep ${AWS_BUCKET}   || ( echo "CREATING BUCKET" ; ${MCBIN} mb ${MYTARGET}/${AWS_BUCKET}    ) && ( echo "BUCKET ALREADY THERE ")
##CREATE USER
#${MCBIN} admin user add ${MYTARGET} ${AWS_USER} ${AWS_AUTH}
${MCBIN} admin user add ${MYTARGET}/${AWS_BUCKET} ${AWS_USER} ${AWS_AUTH}
${MCBIN} admin group add ${MYTARGET} ${AWS_USER}_${AWS_BUCKET} ${AWS_USER}

##ENGAGE POLICY TO USER
${MCBIN} admin policy set ${MYTARGET} ${AWS_USER}_${AWS_BUCKET} user=${AWS_USER}  #${MCBIN} policy set ${AWS_USER}_${AWS_BUCKET} ${MYTARGET}/${AWS_BUCKET}
##ENGAGE POLICY TO GROUP , so user can have multiple buckets
${MCBIN} admin policy set ${MYTARGET} ${AWS_USER}_${AWS_BUCKET} group=${AWS_USER}_${AWS_BUCKET}
